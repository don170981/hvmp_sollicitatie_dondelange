<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title><?php echo lang("site_name"); ?></title>
       
        <?php // Do not use any cached information ?>
        <meta http-equiv="Cache-Control" content="max-age=0"> 
        <meta http-equiv="Cache-Control" content="no-cache, must-revalidate"> 
        <meta http-equiv="Last-Modified" content="<?php gmdate("D, d M Y H:i:s"); ?> GMT">         
        <meta http-equiv="Pragma" content="no-cache">             

        <?php // In case of an IE browser, use the latest rendering engine ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">   
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />   
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="copyright" content="HVMP">
        <meta name="description" content="<?php echo lang("site_name"); ?>">

        <link href="<?php echo $this->config->item('base_url'); ?>/assets/css/main.css" rel="stylesheet" type="text/css">

        <link rel="shortcut icon" href="<?php echo $this->config->item('base_url'); ?>/assets/favicon.ico"> 
    </head>
    <body>
        <div id="container">
           <div id="header"><?php echo lang("site_name"); ?></div>  
           <div id="content"><?php echo $content; ?></div></div>  
           <div id="footer"> <?php echo "Don de Lange"; ?></div>  
       </div>
    </body>
</html>
