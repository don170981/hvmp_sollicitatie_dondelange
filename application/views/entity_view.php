<h1>Entity details</h1>

<?php
    if(isset($entity['name'])){
        echo 'The name of this entity is <b>"'.$entity['name'].'"</b>.';
    }else{
        echo 'This entity is not found.';
    }
?>
<br><br>
<a class="a_button" href="<?php echo $this->config->item('base_url');?>overview">Back to the overview</a> 