<h1>Add entity</h1>

<?php 
    if(strlen(validation_errors())>0){
        echo '<div class="message">'.validation_errors().'</div>';
    }
?>

<form id="addEntityForm" name="addEntityForm" action="add" method="post"> 
    <table>
        <tr>
            <th>Parent</th>
            <td>
                <select id="parent_id" name="parent_id">
            <?php 
                foreach($entity_array as $entity){
                    echo '<option value="'.$entity['id'].'"';
                    if($entity['id'] == $parent_id) echo ' selected';
                    echo '>'.ucfirst($entity['name']).'</option>';
                }
            ?>
                </select>
            </td>
        </tr>
        <tr>
            <th>Type</th>
            <td>
                <?php echo form_dropdown('type', $types, $this->input->post('type')); ?>
            </td>
        </tr>
        <tr>
            <th>Name</th>
            <td>
                <Input type="text" maxlength="25" id="name" name="name">
            </td>
        </tr>        
    </table>

    <input type="hidden" id="validate_form" name="validate_form" value="1">
    <input type="hidden" name="<?php echo $csrf['name'];?>" value="<?php echo $csrf['hash'];?>" />

    <input type="submit" id="submit_add_entity" name="submit_add_entity" label="Add">
    <a class="a_button" href="<?php echo $this->config->item('base_url');?>overview">Cancel</a> 
</form>



<script type="text/javascript">
    // Focus on name
    document.getElementById("name").focus();
</script>