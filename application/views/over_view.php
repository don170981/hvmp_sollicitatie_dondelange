<h1>Overview <a href="entity/reset_list" >Click here for a new set of<br>1000 random entities</a></h1>
<?php  echo $overview_html; ?>

<form id="add_entity_form" name="add_entity_form" action="entity/add" method="post">
    <input type="hidden" id="parent_id" name="parent_id">
    <input type="hidden" name="<?php echo $csrf['name'];?>" value="<?php echo $csrf['hash'];?>" />
</form>

<script type="text/javascript">
    function submitAddEntity(parentId){
        // Set the parentId input value
        document.getElementById('parent_id').value = parentId;
        // Submit the form
        document.getElementById("add_entity_form").submit();
    }
</script>