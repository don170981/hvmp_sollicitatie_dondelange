 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Entity_model extends CI_Model {
    
    /**
     * Entity model
     * 
     * @author    Don de Lange
     */
     
    private $types = array(0 => 'group', 1 => 'item');
    private $html_list = '';
    
    function __construct(){
        parent::__construct();        
    }  

    /*
    * Creates a dummy set of data, containing a variable number of entities.
    */
    function create_dummy_list(){
        $nr_entities = 1000;
        $entities = array ();
        for ($id=0;$id<$nr_entities; $id++) {
            // Add the entity
            $type = rand(0,sizeof($this->types)-1);

            // Determine the parent id
            // Default (when it's the first entity)
            $parent_id = null; 
             
            // Random
            if(sizeof($entities)>0){
                // Create aray of groups
                $groups = array();
                for ($id2=0;$id2<sizeof($entities); $id2++) {
                    if($entities[$id2]['type']==0) array_push($groups, $entities[$id2]['id']);
                }
                
                // Determine the parent id
                if(sizeof($groups)>0){
                    $parent_id = rand(0, 1) ? null : $groups[rand(0,sizeof($groups)-1)];
                }
            }

            // Add the entity to the array
            array_push($entities, array('id' => $id, 'type' => $type, 'parent_id' => $parent_id, 'name' => ucfirst($this->types[$type]).' entity with Id '.$id));
        }
        
        // Save it to the session
        $this->session->set_userdata('entities', $entities);
                                
        return true;
    }

    /*
    * Get an entity by its id
    */
    function get_entity_by_id($entity_id){
        foreach ($this->session->userdata('entities') as $entity){
            if($entity['id'] == $entity_id){
                return $entity;
            }
        }
        return FALSE;
    }

    /*
    * Get the list of entities as an array (as stored in the session)
    * Optionally, filtered by $type.
    */
    function get_list_as_array($type = null){
        if($type === null){
            // If no type is given, return the entire array
            return $this->session->userdata('entities');
        }else{
            // Else, filter (return may be an empty array)
            $return_array = array();
            foreach($this->session->userdata('entities') as $entity){
                if($entity['type'] === $type){
                    array_push($return_array, $entity);
                }
            }
            return $return_array;
        }
    }

    /*
    * Get the list of entities as an hierarchical array
    */
    function get_list_as_hierarchical_array(){
        return $this->construct_array($this->session->userdata('entities'));
    }

    /*
    * Get the list of entities as an hierarchical HTML list
    */
    function get_list_as_html(){
        $this->html_list = '<ul>'; 
        $this->get_list_as_html_per_level($this->get_list_as_hierarchical_array());
        return $this->html_list;
    }
    
    /*
    * Get the list of entities as an hierarchical HTML list (per level)
    */
    private function get_list_as_html_per_level($array, $level = 0, $html = ''){     
        foreach($array as $entity){
            
            switch($entity['type']){
                case 0:     // Group
                    $this->html_list .= '<li class='.$this->types[$entity['type']].'><span>'.$entity['name'].'</span>';
                    $this->html_list .= '<span class="button" title="Add an entity to this group" onClick="submitAddEntity('.$entity['id'].')">+</span></li>';
                    break;

                case 1:     // Item
                    $this->html_list .= '<li class='.$this->types[$entity['type']].'>';
                    $this->html_list .= '<a href="entity/view/'.$entity['id'].'">'.$entity['name'].'</a></li>';
                    break;
            }

            if(isset($entity['children'])) {
                $this->html_list .= '<ul>';
                $this->get_list_as_html_per_level($entity['children'], $level + 1); 
            }
        }
        $this->html_list .= '</ul>';     
    }
    
    function construct_array($elements, $parentId = null){
        $array = array();

        // Loop entities
        foreach($elements as $element){
            if ($element['parent_id'] === $parentId){
                $children = $this->construct_array($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $array[] = $element;
            }
        }
        return $array;
    }
    
    function get_types(){
        return $this->types;
    }

    function add($type, $parent_id, $name){
        // Get the current entity list from the session
        $entities = $this->session->userdata('entities');

        // Add the entity
        array_push($entities, array('id' => sizeof($entities), 'type' => $type, 'parent_id' => $parent_id, 'name' => $name));

        // Save it to the session
        $this->session->set_userdata('entities', $entities);
        
        return true;
    }
}
