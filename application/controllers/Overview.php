<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Overview extends CI_Controller {

	private $overview_data = '';
    private $overview_html = '';
    private $types;
     
    public function __construct(){                
        parent::__construct();

        // Load the entity model
        $this->load->model('entity_model');   
        
        // Get the types
        $this->types = $this->entity_model->get_types();     
     }
     
	public function index(){   
        // Create or read the entity list
        if($this->session->userdata('entities')){
            // Read from the session
            $entities = $this->session->userdata('entities');
        }else{
            // Create a dummy list
            $entities = $this->create_dummy_entity_set();
        }

        // Get the HTML list
        $data['overview_html'] = $this->entity_model->get_list_as_html();

        $data['csrf'] = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        // Write and render the view
		$this->template->write_view('content', 'over_view', $data);
        $this->template->render();
    }
    
    function create_dummy_entity_set(){
        return $this->entity_model->create_dummy_list();    
    }
}
