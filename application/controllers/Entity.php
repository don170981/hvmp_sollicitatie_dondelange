<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entity extends CI_Controller {

     
    public function __construct(){                
        parent::__construct();

        // Load the entity model
        $this->load->model('entity_model');   
        
        // Get the types
        $this->types = $this->entity_model->get_types();     
    }
    
     /*
      * View an entity
      */
	public function view(){
        
        if ($this->uri->segment(3) !== FALSE){
            // Get the entity id
            $id =  $this->security->xss_clean($this->uri->segment(3, 0));            
            
            // Get the enity from the model
            $data['entity'] = $this->entity_model->get_entity_by_id($id);

            // Show view
            $this->template->write_view('content', 'entity_view', $data);
            $this->template->render();
        }else{
            // Redirect to overview page
            redirect('/', 'refresh');
        }   
    }

    /*
     * Add an entity
     */
	public function add(){
        
        if(isset($_POST['parent_id'])){
            // Load the library
            $this->load->library('form_validation');

            // Basic form data
            $data                   = array();
            $data['parent_id']      = $this->security->xss_clean($_POST['parent_id']);
            $data['types']          = $this->types;
            $data['entity_array']   = $this->entity_model->get_list_as_array(0);
            
            if(isset($_POST['validate_form'])){
                // Form options
                $this->form_validation->set_error_delimiters('', '');
                $this->form_validation->set_rules('parent_id', 'Parent', 'required');
                $this->form_validation->set_rules('type', 'Type', 'required');
                $this->form_validation->set_rules('name', 'Name', 'required');
            }

            if ($this->form_validation->run() == FALSE){ 
                // Set csrf info
                $data['csrf'] = array(
                    'name' => $this->security->get_csrf_token_name(),
                    'hash' => $this->security->get_csrf_hash()
                );

                // Show form (possibly with validation errors)
                $this->template->write_view('content', 'entity_add_view', $data);
                $this->template->render();
            }else{
                // Success
                $type       = (int)$this->security->xss_clean($_POST['type']);
                $parent_id  = (int)$this->security->xss_clean($_POST['parent_id']);
                $name       = $this->security->xss_clean($_POST['name']);

                $this->entity_model->add($type, $parent_id, $name);

                // Redirect to overview page
                redirect('/', 'refresh');
            }
        }   
    }
    

    /*
      * Reset the entity list
      */
    public function reset_list(){
        // Create a dummy list
        $entities = $this->entity_model->create_dummy_list();

        // Redirect to overview page
        redirect('/', 'refresh');
    }
}
